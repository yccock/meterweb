package cn.fetosoft.woodpecker.test;

import cn.fetosoft.woodpecker.ApplicationCluster;
import cn.fetosoft.woodpecker.core.pubsub.Command;
import cn.fetosoft.woodpecker.core.pubsub.Publication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 发布测试
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 21:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApplicationCluster.class})
public class ZkPublishTest {

    @Autowired
    private Publication publication;

    @Test
    public void publish() throws Exception {
        Command entity = new Command();
        entity.setCommand(Command.CMD_START_TEST);
        entity.setCmdTime(System.currentTimeMillis());
        publication.publish(entity.toString());
    }
}
