package cn.fetosoft.woodpecker.cluster;

import cn.fetosoft.woodpecker.core.pubsub.cluster.ClusterSubscription;
import cn.fetosoft.woodpecker.core.pubsub.zookeeper.CuratorFrameworkBuilder;
import cn.fetosoft.woodpecker.core.pubsub.zookeeper.ZookeeperCfg;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 12:43
 */
@Configuration
public class SystemConfig {

    @Bean
    @ConfigurationProperties(prefix = "meter.zookeeper")
    public ZookeeperCfg createZookeeperCfg(){
        return new ZookeeperCfg();
    }

    @Bean
    public CuratorFramework createCuratorFramework(ZookeeperCfg cfg){
        return new CuratorFrameworkBuilder(cfg).createFramework();
    }

    @Bean
    public ClusterSubscription createSubscription(){
        return new ClusterSubscription();
    }
}
