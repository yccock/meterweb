#!  /bin/sh
for pid in `ps -ef | grep 'meterweb.war' | grep -v 'grep' | awk '{print $2}'`
do
   kill -9 "${pid}"
done

curPath=$(dirname $(readlink -f "$0"))
java -Xmx512m -Xms512m -jar  $curPath/meterweb.war  > $curPath/log.log &

echo $! > $curPath/pid.pid
