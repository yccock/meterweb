package cn.fetosoft.woodpecker.struct;

import lombok.Getter;
import lombok.Setter;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/16 21:24
 */
@Setter
@Getter
public class UserIdentity {

    private String userId;

    private String username;

    private String name;

    private String mail;

    private String avatar;

    public UserIdentity(String userId, String username){
        this(userId, username, null, null, null);
    }

    public UserIdentity(String userId, String username, String name, String mail, String avatar){
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.mail = mail;
        this.avatar = avatar;
    }
}
