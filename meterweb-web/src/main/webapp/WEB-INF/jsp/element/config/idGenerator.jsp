<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">随机ID生成器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','idGenerator_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#idGenerator_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="idGenerator_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="idGenerator_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="paramNames" style="width:90%" data-options="label:'参数名称：',labelWidth:150">
                        <p style="padding: 3px 0 0 150px;">多个参数以英文“;”隔开</p>
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="mode" style="width:50%;"
                                data-options="label:'随机数类型：',editable:false,panelMaxHeight:150,labelWidth:150">
                            <option value="uuid">32位UUID</option>
                            <option value="datenum">日期+随机数字</option>
                            <option value="number">数字</option>
                            <option value="varnum">字母数字混合</option>
                            <option value="timestampMs">时间戳（毫秒级13位）</option>
                            <option value="timestamp">时间戳（秒级10位）</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="prefix" style="width:90%" data-options="label:'字符串前缀：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="pattern" style="width:90%" data-options="label:'日期格式：',labelWidth:150">
                    </div>
                    <div>
                        <input class="easyui-numberbox" name="length" style="width:90%" data-options="label:'随机数长度：',labelWidth:150">
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','idGenerator_form_',function(eid,status) {
                    if(status===1){
                    menuTree.updateNodeText($('#idGenerator_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>