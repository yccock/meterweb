<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">JSON提取器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','jsonExtract_form_',function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#jsonExtract_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="jsonExtract_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" id="jsonExtract_form_name_${eid}" name="name" style="width:100%"
                               data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:100%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>多个“变量名”、“path表达式”、“默认参数值”用“;”隔开</p>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="referenceNames" style="width:100%" data-options="label:'变量名称：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="jsonPathExprs" style="width:100%" data-options="label:'JSON Path表达式：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="matchNumbers" style="width:100%" data-options="label:'多参匹配原则：',labelWidth:150" value="1">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="defaultValues" style="width:100%" data-options="label:'默认参数值：',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <h4>参数说明：</h4><br>
                    <p>参数名称用于接收提取的参数值，只支持英文、下划线；</p>
                    <p>JSON Path表达式，见文档示例：https://github.com/json-path/JsonPath；</p>
                    <p>多参匹配原则用于提取到多个相同的参数，匹配第几个参数：0表示随机匹配一个，1表示匹配第一个；</p>
                    <p>默认参数值指当未提取到数据时的默认值；</p>
                    <p>“变量名”、“path表达式”、“默认参数值”的个数必须保持一致，否则将出现异常；</p>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','jsonExtract_form_',function(eid, status) {
                        if(status===1){
                            menuTree.updateNodeText($('#jsonExtract_form_name_' + eid).val());
                        }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>