package cn.fetosoft.woodpecker;

import cn.fetosoft.woodpecker.core.data.entity.Report;
import cn.fetosoft.woodpecker.core.data.form.ReportForm;
import cn.fetosoft.woodpecker.core.data.service.ReportService;
import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/6 14:18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApplicationStart.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportTest {

	@Autowired
	private ReportService reportService;

	@Test
	public void findReport(){
		ReportForm form = new ReportForm();
		try{
			form.setStartTime(DateFormatEnum.YMD_HMS.stringToDate("2021-08-06 15:00:00"));
			form.setEndTime(DateFormatEnum.YMD_HMS.stringToDate("2021-08-06 23:59:59"));
			List<Report> list = reportService.selectListByForm(form, Report.class);
			if(!CollectionUtils.isEmpty(list)){
				System.out.println(list.size());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
