package cn.fetosoft.woodpecker.core.data.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @author guobingbing
 * @create 2020/11/30 20:10
 */
@Setter
@Getter
public class BaseEntity {

	@MongoId(targetType = FieldType.OBJECT_ID)
	@DataField(updatable = false)
	private String id;

	/**
	 * 创建时间
	 */
	@Field(targetType = FieldType.DATE_TIME)
	@DataField(updatable = false)
	private Long createTime;

	/**
	 * 修改时间
	 */
	@Field(targetType = FieldType.DATE_TIME)
	@DataField
	private Long modifyTime;
}
