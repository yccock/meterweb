package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.annotation.NotBlank;
import cn.fetosoft.woodpecker.core.data.base.BaseEntity;
import cn.fetosoft.woodpecker.core.data.base.DataField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/10 17:28
 */
@Setter
@Getter
@Document("wp_element")
public class BaseElement extends BaseEntity {

	@NotBlank(message = "项目编号[planId]不能为空")
	@DataField(updatable = false)
	private String planId;

	@NotBlank(message = "类别[category]不能为空")
	@DataField(updatable = false)
	private String category;

	@NotBlank(message = "名称[name]不能为空")
	@DataField
	private String name;

	@DataField
	private String dest;

	@DataField
	private Boolean enabled;

	@NotBlank(message = "父节点ID[parentId]不能为空")
	@DataField
	private String parentId;

	@DataField
	private Integer sort;

	private String testId;

	@NotBlank(message = "UserId不能为空")
	@DataField(updatable = false)
	private String userId;
}
