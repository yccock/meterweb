package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.BaseForm;
import cn.fetosoft.woodpecker.core.data.base.Condition;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/14 21:38
 */
@Setter
@Getter
@Document("wp_user")
public class UserForm extends BaseForm {

    @Condition
    private String userId;

    @Condition
    private String username;

    @Condition
    private String password;
}
