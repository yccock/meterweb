package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.HttpConfigElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import cn.fetosoft.woodpecker.core.jmeter.extension.HttpShareConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建Http默认配置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:31
 */
@Component("httpConfigImpl")
public class CreateHttpConfigImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		HttpConfigElement config = (HttpConfigElement) be;
		HttpShareConfig testElement = new HttpShareConfig();
		if(StringUtils.isNotBlank(config.getProtocol())){
			testElement.setProperty(HTTPSamplerBase.PROTOCOL, config.getProtocol());
		}
		if(StringUtils.isNotBlank(config.getDomain())) {
			testElement.setProperty(HTTPSamplerBase.DOMAIN, config.getDomain());
		}
		if(StringUtils.isNotBlank(config.getPort())){
			testElement.setProperty(HTTPSamplerBase.PORT, config.getPort());
		}else{
			if("https".equalsIgnoreCase(config.getProtocol())){
				testElement.setProperty(HTTPSamplerBase.PORT, "443");
			}else if("http".equalsIgnoreCase(config.getProtocol())){
				testElement.setProperty(HTTPSamplerBase.PORT, "80");
			}
		}
		if(StringUtils.isNotBlank(config.getPath())){
			testElement.setProperty(HTTPSamplerBase.PATH, config.getPath());
		}
		if(StringUtils.isNotBlank(config.getContentEncode())){
			testElement.setProperty(HTTPSamplerBase.CONTENT_ENCODING, config.getContentEncode());
		}
		if(StringUtils.isNotBlank(config.getConnectTimeout())){
			testElement.setProperty(HTTPSamplerBase.CONNECT_TIMEOUT, config.getConnectTimeout());
		}
		if(StringUtils.isNotBlank(config.getResponseTimeout())){
			testElement.setProperty(HTTPSamplerBase.RESPONSE_TIMEOUT, config.getResponseTimeout());
		}
		return testElement;
	}
}
