package cn.fetosoft.woodpecker.core.pubsub;

/**
 * 发布
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 20:54
 */
public interface Publication {

    /**
     * 发布数据
     * @param data
     * @throws Exception
     */
    void publish(String data) throws Exception;
}
