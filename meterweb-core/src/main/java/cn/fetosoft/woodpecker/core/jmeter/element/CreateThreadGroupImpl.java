package cn.fetosoft.woodpecker.core.jmeter.element;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.ThreadGroupElement;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.threads.ThreadGroup;
import org.springframework.stereotype.Component;

/**
 * 创建线程组
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:28
 */
@Component("threadGroupImpl")
public class CreateThreadGroupImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		ThreadGroupElement element = (ThreadGroupElement) be;
		ThreadGroup threadGroup = new ThreadGroup();
		threadGroup.setName(element.getName());
		threadGroup.setComment(element.getDest());
		threadGroup.setEnabled(true);
		threadGroup.setNumThreads(element.getNumThreads());
		threadGroup.setRampUp(element.getRampTime());
		threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
		threadGroup.setScheduler(false);
		threadGroup.setDuration(element.getDuration());
		threadGroup.setDelay(element.getDelay());

		LoopController loopController = new LoopController();
		loopController.setLoops(element.getLoops());
		loopController.setContinueForever(false);
		loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
		loopController.initialize();
		threadGroup.setSamplerController(loopController);
		return threadGroup;
	}
}
