package cn.fetosoft.woodpecker.core.jmeter.element.listener;

import cn.fetosoft.woodpecker.core.data.entity.BaseCollector;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.jmeter.collector.ResultCollectorDump;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.samplers.SampleSaveConfiguration;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 创建结果收集器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:37
 */
@Component("viewResultTreeImpl")
public class CreateViewResultTreeImpl extends AbstractElementBuild implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		BaseCollector collector = (BaseCollector) be;
		//增加结果收集
		Summariser summer = null;
		String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
		if (summariserName.length() > 0) {
			summer = new Summariser(summariserName);
		}
		ResultCollectorDump resultCollector = new ResultCollectorDump(summer, this.applicationContext);
		resultCollector.setProperty(TestElement.TEST_CLASS, ResultCollector.class.getName());
		resultCollector.setErrorLogging(collector.isErrorLogging());
		//resultCollector.setFilename(collector.getFilename());
		//resultCollector.setFilename("F:/jmeter/test5.csv");
		SampleSaveConfiguration saveConfiguration = new SampleSaveConfiguration();
		resultCollector.setSaveConfig(saveConfiguration);
		return resultCollector;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
