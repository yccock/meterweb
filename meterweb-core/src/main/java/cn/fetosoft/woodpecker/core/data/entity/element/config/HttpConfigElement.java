package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseHttp;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Http默认配置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/18 16:43
 */
@Setter
@Getter
@Document("wp_element")
public class HttpConfigElement extends BaseHttp {

}
