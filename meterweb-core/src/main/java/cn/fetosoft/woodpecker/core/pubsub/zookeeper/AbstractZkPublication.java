package cn.fetosoft.woodpecker.core.pubsub.zookeeper;

import cn.fetosoft.woodpecker.core.pubsub.Publication;
import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 发布数据基类
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 20:56
 */
public abstract class AbstractZkPublication extends AbstractZookeeper implements Publication {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractZkPublication.class);

    @Override
    public void publish(String data) throws Exception {
        String path = this.getPublishPath();
        if(StringUtils.isNotBlank(path)){
            this.createNotExist(path, CreateMode.PERSISTENT);
            Stat stat = this.client.setData().forPath(path, data.getBytes(CHARSET));
            LOGGER.info("publish===========>{}",stat.toString());
        }
    }

    /**
     * 获取发布Path
     * @return
     */
    protected abstract String getPublishPath();
}
