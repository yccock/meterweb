package cn.fetosoft.woodpecker.core.jmeter.element.assertion;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.assertion.JsonPathAssertionElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.assertions.JSONPathAssertion;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建json断言
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/16 16:01
 */
@Component("jsonPathAssertionImpl")
public class CreateJsonPathAssertionImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		JsonPathAssertionElement element = (JsonPathAssertionElement) be;
		JSONPathAssertion assertion = new JSONPathAssertion();
		assertion.setJsonPath(element.getJsonPath());
		if(StringUtils.isNotBlank(element.getJsonValidation())){
			assertion.setJsonValidationBool(Boolean.parseBoolean(element.getJsonValidation()));
		}else{
			assertion.setJsonValidationBool(false);
		}
		if(StringUtils.isNotBlank(element.getIsRegex())){
			assertion.setIsRegex(Boolean.parseBoolean(element.getIsRegex()));
		}else{
			assertion.setIsRegex(false);
		}
		assertion.setExpectedValue(element.getExpectedValue());
		if(StringUtils.isNotBlank(element.getExpectNull())){
			assertion.setExpectNull(Boolean.parseBoolean(element.getExpectNull()));
		}else{
			assertion.setExpectNull(false);
		}
		if(StringUtils.isNotBlank(element.getInvert())) {
			assertion.setInvert(Boolean.parseBoolean(element.getInvert()));
		}else{
			assertion.setInvert(false);
		}
		return assertion;
	}
}