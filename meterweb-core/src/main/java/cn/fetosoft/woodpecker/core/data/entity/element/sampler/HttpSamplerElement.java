package cn.fetosoft.woodpecker.core.data.entity.element.sampler;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseHttp;
import cn.fetosoft.woodpecker.core.data.entity.element.MultiArguments;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/21 9:28
 */
@Setter
@Getter
@Document("wp_element")
public class HttpSamplerElement extends BaseHttp implements MultiArguments {

	@DataField
	private String method;
	@DataField
	private boolean followRedirects = true;
	@DataField
	private boolean autoRedirects = false;
	@DataField
	private boolean useKeepalive = true;
	@DataField
	private String implementation = "HttpClient4";
	@DataField
	private String postBodyRaw;
	@DataField
	private String arguments;
	@DataField
	private String[] names;
	@DataField
	private String[] values;

	@Override
	public List<String> getFields() {
		return Arrays.asList("names", "values");
	}
}
