package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.AesSecretElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import cn.fetosoft.woodpecker.core.jmeter.processor.AesSecretProcessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建AES加解密处理器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:54
 */
@Component("aesSecretImpl")
public class CreateAesSecretProcessorImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		AesSecretElement element = (AesSecretElement) be;
		AesSecretProcessor processor = new AesSecretProcessor();
		processor.setCipherMode(element.getCipherMode());
		processor.setFillMode(element.getFillMode());
		processor.setIvOffset(element.getIvOffset());
		processor.setHexKey(element.getHexKey());
		processor.setEncoding(element.getEncoding());
		processor.setEncOrDec(element.getEncOrDec());
		processor.setOutputParams(element.getOutputParams());
		if(StringUtils.isNotBlank(element.getInputParams())){
			processor.setInputParams(element.getInputParams());
			processor.setUsedPrevResult(false);
		}else{
			processor.setUsedPrevResult(true);
		}
		return processor;
	}
}
