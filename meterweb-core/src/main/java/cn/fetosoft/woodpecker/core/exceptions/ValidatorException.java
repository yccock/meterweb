package cn.fetosoft.woodpecker.core.exceptions;

/**
 * 验证异常
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/15 11:22
 */
public class ValidatorException extends Exception {

	public ValidatorException(String message){
		super(message);
	}
}
