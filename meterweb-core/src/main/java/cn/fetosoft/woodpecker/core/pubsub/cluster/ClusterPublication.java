package cn.fetosoft.woodpecker.core.pubsub.cluster;

import cn.fetosoft.woodpecker.core.pubsub.zookeeper.AbstractZkPublication;

/**
 * 发布命令
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/21 10:42
 */
public class ClusterPublication extends AbstractZkPublication {

    private String subPath;

    public ClusterPublication(String subPath){
        this.subPath = subPath;
    }

    @Override
    protected String getPublishPath() {
        return this.subPath;
    }
}
