package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.BeanShellPostElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.extractor.BeanShellPostProcessor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建BeanShell后置处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/10 21:23
 */
@Component("beanShellPostImpl")
public class CreateBeanShellPostImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        BeanShellPostElement element = (BeanShellPostElement) be;
        BeanShellPostProcessor processor = new BeanShellPostProcessor();
        processor.setProperty("script", element.getScript());
        boolean reset = false;
        if(StringUtils.isNotBlank(element.getResetInterpreter())){
            reset = Boolean.parseBoolean(element.getResetInterpreter());
        }
        processor.setProperty("resetInterpreter", reset);
        processor.setProperty("parameters", element.getParameters());
        processor.setProperty("filename", element.getFilename());
        return processor;
    }
}
